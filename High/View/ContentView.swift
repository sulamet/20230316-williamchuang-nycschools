//
//  ContentView.swift
//  High
//
//  Created by Consultant on 3/15/23.
//

import SwiftUI

struct ContentView: View {
    @StateObject var viewModel : SchoolsViewModel
    @State var isErrorOccured: Bool
    
    
    var body: some View {
        NavigationStack{
            VStack(alignment: .leading){
                if (viewModel.customError == .dataNotFound){
                    ProgressView().alert(isPresented: $isErrorOccured) {
                        Alert(title: Text(viewModel.customError?.errorDescription ?? ""), message: Text("Try Again"), dismissButton: .default(Text("Okay")))
                    }
                }else{
                    List{
                        ForEach(viewModel.schoolsList){ school in
                            NavigationLink{
                                SchoolDetailsScreen(schoolData: school, viewModel: SchoolDetailsViewModel(networkManager: NetworkManager()), isErrorOccured: false)
                            } label: {
                                ListCell(schoolData: school)
                            }
                        }
                    }
                    .listStyle(PlainListStyle())
                    .onAppear{
                        Task{
                            await viewModel.getSchoolsList(urlString:APIEndPoints.schoolAPIEndpoint)
                            if(viewModel.customError != nil){
                                isErrorOccured = true
                            }
                        }
                    }
                    
                    .refreshable{
                        await viewModel.getSchoolsList(urlString: APIEndPoints.schoolAPIEndpoint)
                    }
                }
            }
            .navigationTitle(Text("Schools List"))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: SchoolsViewModel(networkManager: NetworkManager()), isErrorOccured: false)
    }
}
