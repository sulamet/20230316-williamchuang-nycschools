//
//  APIEndPoints.swift
//  High
//
//  Created by Consultant on 3/16/23.
//

import Foundation

struct APIEndPoints
{
    static var schoolAPIEndpoint = "https://data.cityofnewyork.us/resource/23z9-6uk9.json"
    static let schoolSatsDetails = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
}
